const http = require("http");
const port = 4000

const server = http.createServer((req,res) => {


if(req.url == "/items" && req.method == "GET") {
	res.writeHead(200,{'Content-Type': 'text/plain'})
    res.end("Data retrieved from database")
}

//Post method
if(req.url == "/items" && req.method == "POST"){
	res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Data to be sent to database')
}

//Put method
if(req.url == "/updateitems" && req.method == "PUT"){
	res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Update our resources')
}

//Delete method

if(req.url == "/deleteitems" && req.method == "DELETE"){
	res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Delete our resources')
}

})
server.listen(port);


console.log(`Server is running at localhost: ${port}`)