//CODE ON HOW TO CREATE DATABASE


const http = require('http');

const port = 5000;

//3. Create a mock database with the data:

const server = http.createServer((req,res) => {;
	
let directory = [
	 {
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
        },
        {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
        }
]

console.log(typeof directory)


//4. Create a route "/profile" and request all the information in the data




if(req.url =='/profile' && req.method == "GET"){
	res.writeHead(200, {'Content-Type': 'application/json'});
	res.write(JSON.stringify(directory))
    res.end()
}


//5. Create a route for creating a new item upon receiving a POST request

if(req.url == '/newprofile' && req.method == "POST"){


//6. For postman, create another collection, export the requests and save to your activity folder

let requestBody = '';


req.on('data', function(data){
      
      requestBody += data; 
});

req.on('end', function(){

	
	requestBody = JSON.parse(requestBody)

//7.Create new object representing the new mock database record

   let newUser = {

        "name": requestBody.name,
        "email": requestBody.email	  
   }

  directory.push(newUser);
  console.log(directory)

  res.writeHead(200, {'Content-Type': 'application/json'})
  res.write(JSON.stringify(newUser))
  res.end()

})


}



});

server.listen(port);

console.log(`Server is running at localhost: ${port}`)

//You can only add one batch